//
//  MCUCommand.swift
//  expecimal
//
//  Created by Eric Betts on 3/8/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation

class MCUCommand {
    var payload : Data = Data()
    
    init(payload: Data) {
        self.payload = payload
    }
    
    func serialize() -> Data {
        var output = Data.init(count: 48)
        output[0] = 0x11
        output[1] = Command.nextSequence
        
        output.replaceSubrange(10..<10+payload.count, with: payload)
        
        //Note that we skip the first byte of the payload
        output[output.count - 1] = crc8(output.subdata(in: 11..<output.count - 1))
        return output
    }
    
    func crc8(_ data: Data) -> UInt8 {
        let polynomial: UInt8 = 0x07
        var accumulator: UInt8 = 0
        
        for byte in data {
            accumulator = accumulator ^ byte
            for _ in 0 ..< 8 {
                if (accumulator & 0x80 != 0) {
                    accumulator = (accumulator << 1) ^ polynomial
                } else {
                    accumulator = accumulator << 1
                }
            }
        }
        return accumulator
    }
}
