//
//  Data+hex.swift
//  expecimal
//
//  Created by Eric Betts on 1/29/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
        static let midcate = HexEncodingOptions(rawValue: 1 << 1)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        if (options.contains(.midcate) && count > 5) {
            var result = ""
            for i in 0..<count {
                if (i < 3) { // Always keep first 3 bytes
                    result += String(format: format, self[i])
                } else if (self[i] == self[i - 1] && self[i - 1] == self[i - 2]) {
                    if (result[result.count-1] != ".") {
                        result += "."
                    }
                } else {
                    result += String(format: format, self[i])
                }
            }
            return result
        }
        return map { String(format: format, $0) }.joined()
    }
    
}
