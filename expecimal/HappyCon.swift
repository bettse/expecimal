//
//  HappyCon.swift
//  expecimal
//
//  Created by Eric Betts on 3/1/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation

class HappyCon {
    enum Battery : UInt8 {
        static func mask (_ x : UInt8) -> UInt8 {
            return x & 0xE0
        }
        init?(value: UInt8) {
            let masked = Battery.mask(value)
            self.init(rawValue: masked)
        }
        case full = 0x80
        case medium = 0x60
        case low = 0x40
        case critical = 0x20
        case empty = 0x00
    }
    
    var counter : UInt8 = 0
    var inputFormat : UInt8 = 0x00
    var vibration : Bool = false
    var lockout : Bool = false
    var battery : Battery = .empty
    var joyconr : JoyConR
    
    init(_ device : JoyConR) {
        joyconr = device
        joyconr.output(Command(subcommand: .getInfo).serialize())
    }
    
    func input(_ data : Data) {
        switch (data[0]) {
        case 0x21: //command ack
            battery = Battery(value: data[2])!
            print("\(battery)🔋 input: [\(data.count)]: \(data.hexEncodedString()) ")
            commandAck(data)
        case 0x30: // update
            parse30Buttons(data.subdata(in: 3..<6))
        case 0x31: // long update
            // parse30Buttons(data.subdata(in: 3..<6))
            parseExtended(data.subdata(in: 49..<data.count))
        case 0x3F: // button pushed
            parse3FButtons(data.subdata(in: 1..<4))
        default:
            print("Other: [\(data.count)]: \(data.hexEncodedString())")
        }
    }
    
    func commandAck(_ message: Data) {
        if (message[13] == 0x00) {
            print("Error: [\(message.count)]: \(message.hexEncodedString())")
        } else {
            let replyTo = message[14]
            print("ACK \(replyTo): \(message.subdata(in: 15..<message.count).hexEncodedString())")
            switch(replyTo) {
            case 0x02:
                print("Request device info")
                joyconr.output(Command(subcommand: .setShipment).serialize())
            case 0x03:
                print("Report change successful \(inputFormat)")
                if (inputFormat == 0x30) {
                    joyconr.output(Command(subcommand: .triggerButtonsElapsedTime).serialize())
                } else if (inputFormat == 0x31) {
                    self.joyconr.output(MCUCommand(payload: Data([0x01])).serialize())
                }
            case 0x04:
                print("Trigger buttons elapsed time")
                joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x80, 0x60, 0x00, 0x00, 0x18])).serialize())
            case 0x08:
                print("Set shipment")
                joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x00, 0x60, 0x00, 0x00, 0x10])).serialize())
            case 0x10:
                if (message[13] == 0x90 && message[15] == 0x00 && message[16] == 0x60) {
                    print("read 0x6000")
                    joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x3d, 0x60, 0x00, 0x00, 0x19])).serialize())
                } else if (message[13] == 0x90 && message[15] == 0x20 && message[16] == 0x60) {
                    print("read 0x6020")
                    vibration = true
                    joyconr.output(Command(subcommand: .enableVibration, parameters: Data([0x01])).serialize())
                } else if (message[13] == 0x90 && message[15] == 0x3d && message[16] == 0x60) {
                    print("read 0x603d")
                    inputFormat = 0x30
                    joyconr.output(Command(subcommand: .setReportMode, parameters: Data([inputFormat])).serialize())                    
                } else if (message[13] == 0x90 && message[15] == 0x80 && message[16] == 0x60) {
                    print("read 0x6080")
                    joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x98, 0x60, 0x00, 0x00, 0x12])).serialize())
                } else if (message[13] == 0x90 && message[15] == 0x98 && message[16] == 0x60) {
                    print("read 0x6098")
                    joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x10, 0x80, 0x00, 0x00, 0x18])).serialize())
                } else if (message[13] == 0x90 && message[15] == 0x10 && message[16] == 0x80) {
                    print("read 0x8010")
                    joyconr.output(Command(subcommand: .readFlash, parameters: Data([0x20, 0x60, 0x00, 0x00, 0x18])).serialize())
                }
            case 0x21:
                print("MCU Config successful")
                if (inputFormat == 0x31) {
                    self.joyconr.output(MCUCommand(payload: Data([0x01])).serialize())
                } else {
                    inputFormat = 0x31
                    joyconr.output(Command(subcommand: .setReportMode, parameters: Data([inputFormat])).serialize())
                }
            case 0x22:
                print("MCU State successful")
                joyconr.output(Command(subcommand: .setMCUConfig, parameters: Data([0x21])).serialize())
            case 0x30:
                print("setPlayerLights successful")
                joyconr.output(Command(subcommand: .setMCUState, parameters: Data([0x01])).serialize())
            case 0x48:
                print("vibration")
                joyconr.output(Command(subcommand: .enableIMU, parameters: Data([0x01])).serialize())
            case 0x40:
                print("IMU")
                joyconr.output(Command(subcommand: .setPlayerLights, parameters: Data([0x08])).serialize())
                
            default: print("ACK \(replyTo): \(message.hexEncodedString())")
            }
        }
    }
    
    func parseExtended(_ data: Data) {
        let poll = NFCCommand(subcommand: .startPolling, parameters: Data([0x00, 0x00, 0x08, 0x05, 0x00, 0x00, 0x00, 0x2c, 0x01]))
        if (data[0] == 0x01) {
            if (data[7] == 0x01) {
                self.joyconr.output(Command(subcommand: .setMCUConfig, parameters: Data([0x21, 0x00, 0x04])).serialize())
            } else if (data[7] == 0x04) {
                self.joyconr.output(MCUCommand(payload: Data([0x02, 0x04, 0, 0, 0x08])).serialize())
            } else {
                self.joyconr.output(MCUCommand(payload: Data([0x01])).serialize())
            }
        } else if (data[0] == 0x2a) { // 2a 00 05 00 00 09 31 09 00 00 00 01 01 02 00 07 04 d4 b1 42 54 49 80 00
            if (data[7] == 0x00) {
                print("Preparing")
                //self.joyconr.output(MCUCommand(payload: Data([0x02, 0x01, 0x00, 0x00, 0x08, 0x05, 0x00, 0x00, 0x00, 0x2c, 0x01])).serialize())
                self.joyconr.output(poll.serialize())
            } else if (data[7] == 0x01) {
                print("Ready to scan")
                self.joyconr.output(poll.serialize())
            } else if (data[7] == 0x09) { //Found one?
                let uidLen = Int(data[15])
                let uidEnd = 16 + uidLen
                let uid = data.subdata(in: 16..<uidEnd)
                print("UID: \(uid.hexEncodedString())")
                //if (data[13] == 0x20) { // Amiibo/ntag
                self.joyconr.output(NFCCommand(subcommand: .read, parameters: Data([0x00, 0x00, 0x08, 0x13, 0xd0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00, 0x3b, 0x3c, 0x77, 0x78, 0x86])).serialize())
                //}
            } else {
                self.joyconr.output(poll.serialize())
            }
        } else if (data[0] == 0x3a) {
            //print("data?")
            self.joyconr.output(poll.serialize())
        } else {
            self.joyconr.output(MCUCommand(payload: Data([0x01])).serialize())
        }
        print("extended data: \(data.hexEncodedString())")
    }
    
    func parse30Buttons(_ buttonData: Data) {
        // print("30 Buttons data: \(buttonData.hexEncodedString())")
        struct RightOptions: OptionSet {
            let rawValue: UInt8
            static let y = RightOptions(rawValue: 0x01)
            static let x = RightOptions(rawValue: 0x02)
            static let b = RightOptions(rawValue: 0x04)
            static let a = RightOptions(rawValue: 0x08)
            static let sr = RightOptions(rawValue: 0x10)
            static let sl = RightOptions(rawValue: 0x20)
            static let r = RightOptions(rawValue: 0x40)
            static let zr = RightOptions(rawValue: 0x80)
        }
        struct SharedOptions: OptionSet {
            let rawValue: UInt8
            static let minus = SharedOptions(rawValue: 0x01)
            static let plus = SharedOptions(rawValue: 0x02)
            static let rstick = SharedOptions(rawValue: 0x04)
            static let lstick = SharedOptions(rawValue: 0x08)
            static let home = SharedOptions(rawValue: 0x10)
            static let capture = SharedOptions(rawValue: 0x20)
            //no 0x40
            static let charginggrip = SharedOptions(rawValue: 0x80)
        }
        struct LeftOptions: OptionSet {
            let rawValue: UInt8
            static let down = LeftOptions(rawValue: 0x01)
            static let up = LeftOptions(rawValue: 0x02)
            static let right = LeftOptions(rawValue: 0x04)
            static let left = LeftOptions(rawValue: 0x08)
            static let sr = LeftOptions(rawValue: 0x10)
            static let sl = LeftOptions(rawValue: 0x20)
            static let l = LeftOptions(rawValue: 0x40)
            static let zl = LeftOptions(rawValue: 0x80)
        }
        
        let right = RightOptions(rawValue: buttonData[0])
        let shared = SharedOptions(rawValue: buttonData[1])
        let left = LeftOptions(rawValue: buttonData[2])
        
        //print(right, shared, left)
    }
    
    func parse3FButtons(_ buttonData: Data) {
        // print("3F Buttons data: \(buttonData.hexEncodedString())")
        struct FirstByteOptions: OptionSet {
            let rawValue: UInt8
            static let down = FirstByteOptions(rawValue: 0x01)
            static let right = FirstByteOptions(rawValue: 0x02)
            static let left = FirstByteOptions(rawValue: 0x04)
            static let up = FirstByteOptions(rawValue: 0x08)
            static let sl = FirstByteOptions(rawValue: 0x10)
            static let sr = FirstByteOptions(rawValue: 0x20)
        }
        struct SecondByteOptions: OptionSet {
            let rawValue: UInt8
            static let minus = SecondByteOptions(rawValue: 0x01)
            static let plus = SecondByteOptions(rawValue: 0x02)
            static let leftStick = SecondByteOptions(rawValue: 0x04)
            static let rightStick = SecondByteOptions(rawValue: 0x08)
            static let home = SecondByteOptions(rawValue: 0x10)
            static let capture = SecondByteOptions(rawValue: 0x20)
            static let lr = SecondByteOptions(rawValue: 0x40)
            static let zlr = SecondByteOptions(rawValue: 0x80)
        }
        
        enum HatDirection : UInt8 {
            case north = 0
            case northeast = 1
            case east = 2
            case southeast = 3
            case south = 4
            case southwest = 5
            case west = 6
            case northwest = 7
            case center = 8
        }
        
        if (FirstByteOptions(rawValue: buttonData[0]).contains(.down)) {
            print("Down")
        }
        
        if let direction = HatDirection(rawValue: buttonData[2]) {
            //print(direction)
        }
    }
}
