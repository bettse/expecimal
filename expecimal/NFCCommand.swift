//
//  NFCCommand.swift
//  expecimal
//
//  Created by Eric Betts on 3/24/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation
class NFCCommand: MCUCommand {
    let NFC_COMMAND : UInt8 = 0x02
    
    enum Subcommand : UInt8 {
        case startPolling = 0x01
        case stopPolling = 0x02
        case read = 0x06
        case write = 0x08
        case raw = 0x09
        case mifare = 0x0f
        case clear = 0x11
    }

    
    convenience init(subcommand: Subcommand) {
        self.init(subcommand: subcommand, parameters: Data())
    }

    
    // The parameters will be something like
    // 0x00, 0x00, 0x08, 0x05, 0x00, 0x00, 0x00, 0x2c, 0x01
    // 0th byte is packet count
    // 0x08 means complete/final packet (0x00 to indicate incomplete)
    // 0x05 is length of following content
    
    //TODO: enhance this to support multi-packet messages
    init(subcommand: Subcommand, parameters: Data) {
        let base = Data(bytes: [NFC_COMMAND, subcommand.rawValue])
        super.init(payload: base + parameters)
    }
}
