//
//  main.swift
//  expecimal
//
//  Created by Eric Betts on 1/29/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation

// https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/blob/master/bluetooth_hid_subcommands_notes.md#subcommand-0x03-set-input-report-mode

let joyconr = JoyConR.singleton
let center = NotificationCenter.default
let mainQueue = OperationQueue.main

// Kick off the state machine
center.addObserver(forName: Notification.Name("deviceConnected"), object: nil, queue: mainQueue) { (note) in
    let happycon = HappyCon(joyconr)

    center.addObserver(forName: Notification.Name("input"), object: nil, queue: mainQueue) { (note) in
        guard let userInfo = note.userInfo else {
            print("No userInfo")
            return
        }
        
        guard let message = userInfo["message"] else {
            print("No message in userInfo")
            return
        }
        
        guard let data = message as? Data else {
            print("Message in not Data")
            return
        }
        
        happycon.input(data)
    }
}

joyconr.initUsb()
