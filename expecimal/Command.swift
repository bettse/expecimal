//
//  Command.swift
//  expecimal
//
//  Created by Eric Betts on 3/1/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation

enum Subcommand : UInt8 {
    case getInfo = 0x02
    case setReportMode = 0x03
    case triggerButtonsElapsedTime = 0x04
    case getPage = 0x05
    case setShipment = 0x08
    case readFlash = 0x10
    case setMCUConfig = 0x21
    case setMCUState = 0x22
    case setPlayerLights = 0x30
    case enableIMU = 0x40
    case enableVibration = 0x48
}

class Command {
    static var corrolationGenerator = (0..<0x0f).makeIterator()
    static var nextSequence : UInt8 {
        get {
            if let next = corrolationGenerator.next() {
                return UInt8(next)
            } else {
                corrolationGenerator = (1..<0x0f).makeIterator()
                return 0
            }
        }
    }
    var subcommand : Subcommand
    var parameters : Data = Data()
    
    init(subcommand: Subcommand) {
        self.subcommand = subcommand
    }

    init(subcommand: Subcommand, parameters: Data) {
        self.subcommand = subcommand
        self.parameters = parameters
    }
    
    // [0x01, 0xFF, 0, 0, 0, 0, 0, 0, 0, 0, 0x02, 0x00, 0x00, 0x00, 0x00],
    func serialize() -> Data {
        var response = Data.init(count: 49) //outputReportSize
        response[0] = 0x01 // All commands are sent to report 1
        response[1] = Command.nextSequence
        response[10] = subcommand.rawValue
        if (parameters.count > 0) {
            response.replaceSubrange(11..<11+parameters.count, with: parameters)
            if (subcommand == .setMCUConfig) {
                //Note that we skip the first byte of the subcommand's params
                response[response.count - 1] = crc8(response.subdata(in: 12..<response.count-1))
            }
        }
        
        return response
    }
    
    func crc8(_ data: Data) -> UInt8 {
        let polynomial: UInt8 = 0x07
        var accumulator: UInt8 = 0

        for byte in data {
            accumulator = accumulator ^ byte
            for _ in 0 ..< 8 {
                if (accumulator & 0x80 != 0) {
                    accumulator = (accumulator << 1) ^ polynomial
                } else {
                    accumulator = accumulator << 1
                }
            }
        }
        return accumulator
    }
}
