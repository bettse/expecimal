//
//  JoyCon-R.swift
//  expecimal
//
//  Created by Eric Betts on 1/29/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Cocoa
import Foundation
import IOKit.hid

let PRO_CONTROLLER = 0x2009

class JoyConR : NSObject {
    let vendorId = 0x057E
    let productId = PRO_CONTROLLER
    let inputReportSize = 362
    let outputReportSize = 49
    static let singleton = JoyConR()
    var device : IOHIDDevice? = nil
    
    func input(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, type: IOHIDReportType, reportId: UInt32, report: UnsafeMutablePointer<UInt8>, reportLength: CFIndex) {
        if (reportId == 0x31 && reportLength == 362 && report[reportLength - 1] == 0x6f) {
            return // Ignore these frequent, long packets when the nfc portion is all 0's
        }
        let message = Data(bytes: report, count: reportLength)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "input"), object: nil, userInfo: ["message": message])
    }
    
    func output(_ data: Data) {
        guard let joyconr = device else {
            print("No device to send to")
            return
        }
        if (data.count > outputReportSize) {
            print("output data too large for USB report")
            return
        }
        let reportId : CFIndex = CFIndex(data[0])
        if (reportId == 0x11) {
            print("Sending \(data.hexEncodedString())")
        }
        IOHIDDeviceSetReport(joyconr, kIOHIDReportTypeOutput, reportId, [UInt8](data), outputReportSize)
    }
    
    func connected(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, inIOHIDDeviceRef: IOHIDDevice!) {
        guard let device = inIOHIDDeviceRef else {
            print("No device to connect to")
            return
        }
        print("Device connected")
        // It would be better to look up the report size and create a chunk of memory of that size
        let reportBuffer = UnsafeMutablePointer<UInt8>.allocate(capacity: inputReportSize)
        self.device = device
        
        if let prop = IOHIDDeviceGetProperty(device, kIOHIDMaxFeatureReportSizeKey as CFString) {
            print ("kIOHIDMaxFeatureReportSizeKey = \(prop)")
        }
        
        if let prop = IOHIDDeviceGetProperty(device, kIOHIDMaxOutputReportSizeKey as CFString) {
            print ("kIOHIDMaxOutputReportSizeKey = \(prop)")
        }
        
        if let prop = IOHIDDeviceGetProperty(device, kIOHIDMaxInputReportSizeKey as CFString) {
            print ("kIOHIDMaxInputReportSizeKey = \(prop)")
        }
        
        if let prop = IOHIDDeviceGetProperty(device, kIOHIDReportDescriptorKey as CFString) {
            print ("kIOHIDReportDescriptorKey = \(prop)")
        }
        
        let inputCallback : IOHIDReportCallback = { inContext, inResult, inSender, type, reportId, report, reportLength in
            let this : JoyConR = Unmanaged<JoyConR>.fromOpaque(inContext!).takeUnretainedValue()
            this.input(inResult, inSender: inSender!, type: type, reportId: reportId, report: report, reportLength: reportLength)
        }
        
        //Hook up inputcallback
        let this = Unmanaged.passRetained(self).toOpaque()
        IOHIDDeviceRegisterInputReportCallback(device, reportBuffer, inputReportSize, inputCallback, this)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "deviceConnected"), object: nil, userInfo: ["class": NSStringFromClass(type(of: self))])
    }
    
    func removed(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, inIOHIDDeviceRef: IOHIDDevice!) {
        print("Device removed")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "deviceDisconnected"), object: nil, userInfo: ["class": NSStringFromClass(type(of: self))])
    }
    
    @objc func initUsb() {
        let deviceMatch = [kIOHIDProductIDKey: productId, kIOHIDVendorIDKey: vendorId]
        let managerRef = IOHIDManagerCreate(kCFAllocatorDefault, IOOptionBits(kIOHIDOptionsTypeNone))
        
        IOHIDManagerSetDeviceMatching(managerRef, deviceMatch as CFDictionary?)
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetCurrent(), CFRunLoopMode.defaultMode.rawValue)
        IOHIDManagerOpen(managerRef, 0)
        
        let matchingCallback : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : JoyConR = Unmanaged<JoyConR>.fromOpaque(inContext!).takeUnretainedValue()
            this.connected(inResult, inSender: inSender!, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        let removalCallback : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : JoyConR = Unmanaged<JoyConR>.fromOpaque(inContext!).takeUnretainedValue()
            this.removed(inResult, inSender: inSender!, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        let this = Unmanaged.passRetained(self).toOpaque()
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, matchingCallback, this)
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, removalCallback, this)
        
        RunLoop.current.run()
    }
    
}

