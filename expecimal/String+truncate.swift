//
//  String+truncate.swift
//  expecimal
//
//  Created by Eric Betts on 2/20/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation
extension String {
    /*
     Truncates the string to the specified length number of characters and appends an optional trailing string if longer.
     - Parameter length: Desired maximum lengths of a string
     - Parameter trailing: A 'String' that will be appended after the truncation.
     
     - Returns: 'String' object.
     */
    func truncate(length: Int, trailing: String = "…") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    // look for runs of a character
    func midcate() -> String {
        if (self.count < 5) {
            return self
        }
        var result = ""
        for i in 0..<self.count {
            if (i < 3) { // Always keep first 3 characters
                result += self[i]
            } else if (String(self[i]) == String(self[i - 1]) && String(self[i - 1]) == String(self[i - 2])) { //repeated chracter
                if (result[result.count-1] != ".") {
                    result += "."
                }
            } else {
                result += self[i]
            }
        }
        return result
    }
}
